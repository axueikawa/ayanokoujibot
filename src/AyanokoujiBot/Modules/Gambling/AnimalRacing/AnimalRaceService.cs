#nullable disable
using AyanokoujiBot.Modules.Gambling.Common.AnimalRacing;

namespace AyanokoujiBot.Modules.Gambling.Services;

public class AnimalRaceService : INService
{
    public ConcurrentDictionary<ulong, AnimalRace> AnimalRaces { get; } = new();
}