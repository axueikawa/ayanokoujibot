#nullable disable
namespace AyanokoujiBot.Modules.Gambling.Common.Waifu;

public enum AffinityTitle
{
    Pure,
    Faithful,
    Playful,
    Cheater,
    Tainted,
    Corrupted,
    Lewd,
    Sloot,
    Depraved,
    Harlot
}