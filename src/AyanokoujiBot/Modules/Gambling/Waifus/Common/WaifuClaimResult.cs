#nullable disable
namespace AyanokoujiBot.Modules.Gambling.Common.Waifu;

public enum WaifuClaimResult
{
    Success,
    NotEnoughFunds,
    InsufficientAmount
}