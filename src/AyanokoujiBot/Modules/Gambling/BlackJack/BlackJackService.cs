#nullable disable
using AyanokoujiBot.Modules.Gambling.Common.Blackjack;

namespace AyanokoujiBot.Modules.Gambling.Services;

public class BlackJackService : INService
{
    public ConcurrentDictionary<ulong, Blackjack> Games { get; } = new();
}