#nullable disable
namespace AyanokoujiBot.Modules.Utility.Common;

public enum StreamRoleListType
{
    Whitelist,
    Blacklist
}