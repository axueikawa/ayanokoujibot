﻿// ReSharper disable InconsistentNaming
namespace AyanokoujiBot.Modules.Utility.Patronage;

public enum PatronTier
{
    None,
    I,
    V,
    X,
    XX,
    L,
    C,
    ComingSoon
}