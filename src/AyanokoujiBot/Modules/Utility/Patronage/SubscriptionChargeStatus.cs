﻿#nullable disable
namespace AyanokoujiBot.Modules.Utility;

public enum SubscriptionChargeStatus
{
    Paid,
    Refunded,
    Unpaid,
    Other,
}