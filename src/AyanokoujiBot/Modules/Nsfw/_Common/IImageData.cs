#nullable disable
namespace AyanokoujiBot.Modules.Nsfw.Common;

public interface IImageData
{
    ImageData ToCachedImageData(Booru type);
}