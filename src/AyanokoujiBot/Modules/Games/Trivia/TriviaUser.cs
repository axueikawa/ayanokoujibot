﻿namespace AyanokoujiBot.Modules.Games.Common.Trivia;

public record class TriviaUser(string Name, ulong Id);