#nullable disable
namespace AyanokoujiBot.Modules.Games.Common.ChatterBot;

public interface IChatterBotSession
{
    Task<string> Think(string input);
}