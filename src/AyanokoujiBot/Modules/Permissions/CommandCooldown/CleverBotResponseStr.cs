﻿#nullable disable
using System.Runtime.InteropServices;

namespace AyanokoujiBot.Modules.Permissions;

[StructLayout(LayoutKind.Sequential, Size = 1)]
public readonly struct CleverBotResponseStr
{
    public const string CLEVERBOT_RESPONSE = "cleverbot:response";
}