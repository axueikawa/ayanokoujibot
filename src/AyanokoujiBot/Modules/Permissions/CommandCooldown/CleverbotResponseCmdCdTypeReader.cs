﻿#nullable disable
using AyanokoujiBot.Common.TypeReaders;
using static AyanokoujiBot.Common.TypeReaders.TypeReaderResult;

namespace AyanokoujiBot.Modules.Permissions;

public class CleverbotResponseCmdCdTypeReader : AyanokoujiTypeReader<CleverBotResponseStr>
{
    public override ValueTask<TypeReaderResult<CleverBotResponseStr>> ReadAsync(
        ICommandContext ctx,
        string input)
        => input.ToLowerInvariant() == CleverBotResponseStr.CLEVERBOT_RESPONSE
            ? new(FromSuccess(new CleverBotResponseStr()))
            : new(FromError<CleverBotResponseStr>(CommandError.ParseFailed, "Not a valid cleverbot"));
}