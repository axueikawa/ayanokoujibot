﻿// ReSharper disable InconsistentNaming
namespace AyanokoujiBot.Modules.Searches;

public enum WebSearchEngine
{
    Google,
    Google_Scrape,
    Searx,
}