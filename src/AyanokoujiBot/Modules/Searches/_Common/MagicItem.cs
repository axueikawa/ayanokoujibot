#nullable disable
namespace AyanokoujiBot.Modules.Searches.Common;

public class MagicItem
{
    public string Name { get; set; }
    public string Description { get; set; }
}