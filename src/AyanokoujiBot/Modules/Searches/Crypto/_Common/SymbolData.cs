﻿namespace AyanokoujiBot.Modules.Searches;

public record SymbolData(string Symbol, string Description);