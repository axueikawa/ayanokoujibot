﻿namespace AyanokoujiBot.Modules.AyanokoujiExpressions;

public enum ExprField
{
    AutoDelete,
    DmResponse,
    AllowTarget,
    ContainsAnywhere,
    Message
}