namespace AyanokoujiBot.Modules.Xp.Services;

public enum ClubUnbanResult
{
    Success,
    NotOwnerOrAdmin,
    WrongUser
}