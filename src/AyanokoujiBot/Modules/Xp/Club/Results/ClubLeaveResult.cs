namespace AyanokoujiBot.Modules.Xp.Services;

public enum ClubLeaveResult
{
    Success,
    OwnerCantLeave,
    NotInAClub
}