namespace AyanokoujiBot.Modules.Xp.Services;

public enum ClubAcceptResult
{
    Accepted,
    NotOwnerOrAdmin,
    NoSuchApplicant,
}