namespace AyanokoujiBot.Modules.Xp.Services;

public enum ClubTransferError
{
    NotOwner,
    TargetNotMember
}