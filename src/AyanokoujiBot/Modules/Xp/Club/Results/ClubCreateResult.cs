namespace AyanokoujiBot.Modules.Xp.Services;

public enum ClubCreateResult
{
    Success,
    AlreadyInAClub,
    NameTaken,
    InsufficientLevel,
}