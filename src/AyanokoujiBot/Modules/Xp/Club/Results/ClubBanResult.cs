namespace AyanokoujiBot.Modules.Xp.Services;

public enum ClubBanResult
{
    Success,
    NotOwnerOrAdmin,
    WrongUser,
    Unbannable,
    
}