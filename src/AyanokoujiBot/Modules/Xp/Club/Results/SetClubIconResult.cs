namespace AyanokoujiBot.Modules.Xp.Services;

public enum SetClubIconResult
{
    Success,
    InvalidFileType,
    TooLarge,
    NotOwner,
}