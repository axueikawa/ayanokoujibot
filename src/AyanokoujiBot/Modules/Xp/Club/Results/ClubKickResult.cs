namespace AyanokoujiBot.Modules.Xp.Services;

public enum ClubKickResult
{
    Success,
    NotOwnerOrAdmin,
    TargetNotAMember,
    Hierarchy
}