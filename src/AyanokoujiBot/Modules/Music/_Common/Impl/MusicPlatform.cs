#nullable disable
namespace AyanokoujiBot.Modules.Music;

public enum MusicPlatform
{
    Radio,
    Youtube,
    Local,
    SoundCloud
}