#nullable disable
namespace AyanokoujiBot.Modules.Music;

public interface ILocalTrackResolver : IPlatformQueryResolver
{
    IAsyncEnumerable<ITrackInfo> ResolveDirectoryAsync(string dirPath);
}