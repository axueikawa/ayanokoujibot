// global using System.Collections.Concurrent;
global using NonBlocking;

// packages
global using Serilog;
global using Humanizer;

// ayanokoujibot
global using AyanokoujiBot;
global using AyanokoujiBot.Services;
global using Ayanokouji.Common; // new project
global using AyanokoujiBot.Common; // old + ayanokoujibot specific things
global using AyanokoujiBot.Common.Attributes;
global using AyanokoujiBot.Extensions;
global using Ayanokouji.Snake;

// discord
global using Discord;
global using Discord.Commands;
global using Discord.Net;
global using Discord.WebSocket;

// aliases
global using GuildPerm = Discord.GuildPermission;
global using ChannelPerm = Discord.ChannelPermission;
global using BotPermAttribute = Discord.Commands.RequireBotPermissionAttribute;
global using LeftoverAttribute = Discord.Commands.RemainderAttribute;
global using TypeReaderResult = AyanokoujiBot.Common.TypeReaders.TypeReaderResult;

// non-essential
global using JetBrains.Annotations;