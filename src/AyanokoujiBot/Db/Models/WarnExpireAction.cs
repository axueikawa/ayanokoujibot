#nullable disable
namespace AyanokoujiBot.Services.Database.Models;

public enum WarnExpireAction
{
    Clear,
    Delete
}