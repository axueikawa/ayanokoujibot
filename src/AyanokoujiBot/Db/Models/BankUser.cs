﻿using AyanokoujiBot.Services.Database.Models;

namespace AyanokoujiBot.Db.Models;

public class BankUser : DbEntity
{
    public ulong UserId { get; set; }
    public long Balance { get; set; }
}