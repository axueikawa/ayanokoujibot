﻿#nullable disable
using LinqToDB;
using Microsoft.EntityFrameworkCore;
using AyanokoujiBot.Services.Database.Models;

namespace AyanokoujiBot.Db;

public static class AyanokoujiExpressionExtensions
{
    public static int ClearFromGuild(this DbSet<AyanokoujiExpression> exprs, ulong guildId)
        => exprs.Delete(x => x.GuildId == guildId);

    public static IEnumerable<AyanokoujiExpression> ForId(this DbSet<AyanokoujiExpression> exprs, ulong id)
        => exprs.AsNoTracking().AsQueryable().Where(x => x.GuildId == id).ToList();
}