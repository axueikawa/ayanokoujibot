using AyanokoujiBot.Services.Currency;

namespace AyanokoujiBot.Services;

public interface ITxTracker
{
    Task TrackAdd(long amount, TxData? txData);
    Task TrackRemove(long amount, TxData? txData);
}