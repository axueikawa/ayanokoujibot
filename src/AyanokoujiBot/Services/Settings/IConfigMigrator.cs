#nullable disable
namespace AyanokoujiBot.Services;

public interface IConfigMigrator
{
    public void EnsureMigrated();
}