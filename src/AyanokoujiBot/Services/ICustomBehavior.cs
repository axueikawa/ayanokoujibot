﻿using AyanokoujiBot.Common.ModuleBehaviors;

namespace AyanokoujiBot.Services;

public interface ICustomBehavior
    : IExecOnMessage,
        IInputTransformer,
        IExecPreCommand,
        IExecNoCommand,
        IExecPostCommand
{

}