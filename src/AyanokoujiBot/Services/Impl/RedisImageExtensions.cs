#nullable disable
namespace AyanokoujiBot.Services;

public static class RedisImageExtensions
{
    private const string OLD_CDN_URL = "ayanokouji-pictures.nyc3.digitaloceanspaces.com";
    private const string NEW_CDN_URL = "cdn.ayanokouji.bot";

    public static Uri ToNewCdn(this Uri uri)
        => new(uri.ToString().Replace(OLD_CDN_URL, NEW_CDN_URL));
}