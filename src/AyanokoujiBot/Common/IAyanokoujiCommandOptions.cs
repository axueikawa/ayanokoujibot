﻿#nullable disable
namespace AyanokoujiBot.Common;

public interface IAyanokoujiCommandOptions
{
    void NormalizeOptions();
}