﻿#nullable disable
namespace AyanokoujiBot.Common;

public interface ICloneable<T>
    where T : new()
{
    public T Clone();
}