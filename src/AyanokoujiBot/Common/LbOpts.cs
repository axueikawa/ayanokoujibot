﻿#nullable disable
using CommandLine;

namespace AyanokoujiBot.Common;

public class LbOpts : IAyanokoujiCommandOptions
{
    [Option('c', "clean", Default = false, HelpText = "Only show users who are on the server.")]
    public bool Clean { get; set; }

    public void NormalizeOptions()
    {
    }
}