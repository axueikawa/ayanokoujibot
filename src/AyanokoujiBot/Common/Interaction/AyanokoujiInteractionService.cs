﻿namespace AyanokoujiBot;

public class AyanokoujiInteractionService : IAyanokoujiInteractionService, INService
{
    private readonly DiscordSocketClient _client;

    public AyanokoujiInteractionService(DiscordSocketClient client)
    {
        _client = client;
    }

    public AyanokoujiInteraction Create<T>(
        ulong userId,
        SimpleInteraction<T> inter)
        => new AyanokoujiInteraction(_client,
            userId,
            inter.Button,
            inter.TriggerAsync,
            onlyAuthor: true);
}