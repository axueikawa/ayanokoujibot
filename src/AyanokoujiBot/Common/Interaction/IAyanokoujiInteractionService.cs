﻿namespace AyanokoujiBot;

public interface IAyanokoujiInteractionService
{
    public AyanokoujiInteraction Create<T>(
        ulong userId,
        SimpleInteraction<T> inter);
}