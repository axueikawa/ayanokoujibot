﻿#nullable disable
namespace AyanokoujiBot.Common.Pokemon;

public class PokemonNameId
{
    public int Id { get; set; }
    public string Name { get; set; }
}