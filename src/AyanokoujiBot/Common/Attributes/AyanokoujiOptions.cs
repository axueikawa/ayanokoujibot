namespace AyanokoujiBot.Common.Attributes;

[AttributeUsage(AttributeTargets.Method)]
public sealed class AyanokoujiOptionsAttribute : Attribute
{
    public Type OptionType { get; set; }

    public AyanokoujiOptionsAttribute(Type t)
        => OptionType = t;
}