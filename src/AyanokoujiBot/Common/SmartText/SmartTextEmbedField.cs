﻿#nullable disable
namespace AyanokoujiBot;

public class SmartTextEmbedField
{
    public string Name { get; set; }
    public string Value { get; set; }
    public bool Inline { get; set; }
}