﻿using System.Runtime.CompilerServices;

namespace Ayanokouji.Medusa;

public class MedusaServiceProvider : IServiceProvider
{
    private readonly IServiceProvider _ayanokoujiServices;
    private readonly IServiceProvider _medusaServices;

    public MedusaServiceProvider(IServiceProvider ayanokoujiServices, IServiceProvider medusaServices)
    {
        _ayanokoujiServices = ayanokoujiServices;
        _medusaServices = medusaServices;
    }

    [MethodImpl(MethodImplOptions.NoInlining)]
    public object? GetService(Type serviceType)
    {
        if (!serviceType.Assembly.IsCollectible)
            return _ayanokoujiServices.GetService(serviceType);

        return _medusaServices.GetService(serviceType);
    }
}