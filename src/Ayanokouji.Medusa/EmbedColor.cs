﻿namespace AyanokoujiBot;

public enum EmbedColor
{
    Ok,
    Pending,
    Error
}