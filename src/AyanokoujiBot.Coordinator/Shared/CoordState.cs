﻿using System.Collections.Generic;

namespace AyanokoujiBot.Coordinator
{
    public class CoordState
    {
        public List<JsonStatusObject> StatusObjects { get; init; }
    }
}