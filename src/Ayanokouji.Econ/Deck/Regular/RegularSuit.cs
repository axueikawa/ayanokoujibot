﻿namespace Ayanokouji.Econ;

public enum RegularSuit
{
    Hearts,
    Diamonds,
    Clubs,
    Spades
}