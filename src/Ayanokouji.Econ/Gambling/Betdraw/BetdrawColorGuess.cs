﻿namespace Ayanokouji.Econ.Gambling.Betdraw;

public enum BetdrawColorGuess
{
    Red,
    Black
}