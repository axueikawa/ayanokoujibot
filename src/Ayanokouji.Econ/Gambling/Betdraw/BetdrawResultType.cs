﻿namespace Ayanokouji.Econ.Gambling.Betdraw;

public enum BetdrawResultType
{
    Win,
    Lose
}